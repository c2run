PREFIX ?= /usr/local
DESTDIR ?=
BINDIR ?= $(PREFIX)/bin
MANDIR ?= $(PREFIX)/share/man

CFLAGS ?= -O3 -march=native
CFLAGS += -std=gnu99 -Wall -Wextra -Wconversion -D_GNU_SOURCE

PROG = c2run

all: $(PROG)

$(PROG):

install: $(PROG)
	@install -v -d "$(DESTDIR)$(BINDIR)" && install -v -m 0755 $(PROG) "$(DESTDIR)$(BINDIR)/$(PROG)"

fmt:
	clang-format -i $(PROG).c

clean:
	rm -f $(PROG)

.PHONY: all clean install fmt
