/* SPDX-License-Identifier: GPL-2.0
 *
 * Copyright (C) 2021 Julien Rische <jrische@laposte.net>.
 * Copyright (C) 2018-2021 Jason A. Donenfeld <Jason@zx2c4.com>.
 * All Rights Reserved.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdbool.h>

typedef const char *err_t;

const char *header = "#include <stddef.h>\n"
                     "#include <stdint.h>\n"
                     "#include <inttypes.h>\n"
                     "#include <stdbool.h>\n"
                     "#include <string.h>\n"
                     "#include <assert.h>\n"
                     "#include <errno.h>\n"
                     "#include <unistd.h>\n"
                     "#include <pthread.h>\n"
                     "#include <limits.h>\n"
                     "#include <time.h>\n"
                     "#include <fcntl.h>\n"
                     "#include <stdio.h>\n"
                     "#include <stdlib.h>\n"
                     "int main(int argc, char *argv[], char *envp[]) {\n"
                     "(void) argc;\n"
                     "(void) argv;\n"
                     "(void) envp;\n";

void exec_cc(int pipe, char *output_path, size_t argc, char *argv[])
{
    size_t cc_argc;
    char **cc_argv;
    char *cc = getenv("CC") ?: "gcc";

    if (dup2(pipe, 0) < 0) {
        perror("Error: unable to duplicate pipe fd");
        _exit(1);
    }

    cc_argc = argc + 6;
    cc_argv = malloc(cc_argc * sizeof(*cc_argv));
    if (!cc_argv) {
        perror("Error: cannot allocation compiler argument list");
        _exit(1);
    }

    memcpy(cc_argv, (char *[]){cc, "-xc", "-o", output_path}, 4 * sizeof(char *));
    memcpy(cc_argv + 4, argv, argc * sizeof(*argv));
    memcpy(cc_argv + (cc_argc - 2), (char *[]){"-", NULL}, 2 * sizeof(char *));

    execvp(cc, cc_argv);
    free(cc_argv);
    _exit(1);
}

err_t pipe_source(int pipe, const char *src_path, int input)
{
    ssize_t len;

    if (src_path) {
        char beginning[2];

        len = read(input, beginning, 2);
        if (len < 0)
            return "Error: unable to read from input file";
        else if (len == 2 && beginning[0] == '#' && beginning[1] == '!')
            len = write(pipe, "//", 2);
        else if (len > 0)
            len = write(pipe, beginning, (size_t)len);
        if (len < 0)
            return "Error: unable to write input preamble";
    } else {
        len = write(pipe, header, strlen(header));
        if (len < 0)
            return "Error: unable to write input header";
    }
    if (splice(input, NULL, pipe, NULL, 0x7fffffff, 0) < 0)
        return "Error: unable to splice input to compiler child";
    if (!src_path) {
        len = write(pipe, "}", 1);
        if (len < 0)
            return "Error: unable to write input footer";
    }
    close(pipe);

    return 0;
}

int main(int argc, char *argv[], char *envp[])
{
    err_t err;
    int ret = 0;
    size_t cc_argc = (size_t)argc - 1;
    char **cc_argv, **esc_argv = (char *[]){argv[0], NULL};

    int fd, input = STDIN_FILENO, pipes[2] = {0};
    pid_t cc_pid;
    char *arg, *output_path, *src_path = NULL;

    cc_argv = argv + 1;
    for (size_t i = 1; arg = argv[i], i < (size_t)argc; ++i) {
        bool cc_param = arg[0] == '-';
        if (!cc_param)
            src_path = arg;
        if (!cc_param || strncmp("-", arg, 2) == 0) {
            cc_argc = i - 1;
            esc_argv = argv + i;
            break;
        }
    }

    if (src_path) {
        input = open(src_path, O_RDONLY);
        if (input < 0) {
            perror("Error: unable to open input file");
            ret = 1;
            goto end;
        }
    }

    if (pipe(pipes) < 0) {
        perror("Error: unable to open filter pipe");
        ret = 1;
        goto end;
    }

    fd = memfd_create("c2run", 0);
    if (fd < 0) {
        perror("Error: unable to create memfd");
        ret = 1;
        goto end;
    }
    if (asprintf(&output_path, "/proc/self/fd/%d", fd) < 0) {
        perror("Error: unable to allocate memory for fd string");
        ret = 1;
        goto end;
    }

    cc_pid = fork();
    if (cc_pid < 0) {
        perror("Error: unable to fork for compiler");
        ret = 1;
        goto end;
    }

    if (cc_pid == 0) {
        close(input);
        close(pipes[1]);
        exec_cc(pipes[0], output_path, cc_argc, cc_argv);
    }

    close(pipes[0]);
    err = pipe_source(pipes[1], src_path, input);
    if (err) {
        perror(err);
        ret = 1;
        goto end;
    }

    {
        int status;
        if (waitpid(cc_pid, &status, 0) != cc_pid || (!WIFEXITED(status) || WEXITSTATUS(status))) {
            fprintf(stderr, "Error: compiler process did not complete successfully\n");
            ret = 1;
            goto end;
        }
    }

    if (fexecve(fd, esc_argv, envp) < 0) {
        perror("Error: could not execute compiled program");
        ret = 1;
        goto end;
    }

end:
    close(input);
    close(pipes[0]);
    close(pipes[1]);
    return ret;
}
